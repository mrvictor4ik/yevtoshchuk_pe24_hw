let mathOperation = function (firstNum, secondNum, operator) {
    firstNum = prompt("Введите первое число");
    while (isNaN(firstNum) || firstNum === null || firstNum === "") {
        firstNum = prompt("Введите первое число", "Например: 10");
    }
    secondNum = prompt("Введите второе число");
    while (isNaN(secondNum) || secondNum === null || secondNum === "") {
        secondNum = prompt("Введите второе число", "Например: 5");
    }
    operator = prompt("Введите знак математической операции");
    while (operator !== `+` && operator !== `-` && operator !== `/` && operator !== `*` || operator === "" || operator === null) {
        operator = prompt("Введите знак математической операции", "Например: +");
    }
    switch (operator) {
        case '+':
            return +`${firstNum}` + +`${secondNum}`;
            break;
        case '-':
            return `${firstNum}` - `${secondNum}`;
            break;
        case '*':
            return `${firstNum}` * `${secondNum}`;
            break;
        case '/':
            return `${firstNum}` / `${secondNum}`;
    }
}
console.log(mathOperation());