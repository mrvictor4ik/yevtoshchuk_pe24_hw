"use strict";

let userNumber;
do {
  userNumber = prompt("Введите число");
} while (Number(userNumber) !== userNumber && userNumber % 1 !== 0);
if (userNumber < 5) {
  console.log(`Sorry, no numbers`);
} else {
  for (let index = 1; index <= userNumber; index++) {
    if (index % 5 === 0) {
      console.log(index);
    }
  }
}

// let m = prompt("Введите первое число");
// let n = prompt("Введите второе число");

// for (let i = 2, max = Math.sqrt(i); i <= max; i++); {
//     if (m % i === 0 && n % i === 0) {
//         console.log(m, n);
//     };
//     console.log(m, n);
// };
